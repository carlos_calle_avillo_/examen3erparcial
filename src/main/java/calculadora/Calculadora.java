package calculadora;

public class Calculadora {
    private String digito;
    private double resultado;
    private boolean operacionSuma;
    private boolean operacionResta;
    private boolean operacionMultiplicacion;
    private boolean operacionDivision;
    
    public Calculadora(){
        this.digito = "";
        this.resultado = 0;
        this.operacionSuma = false;
        this.operacionResta = false;
        this.operacionMultiplicacion = false;
        this.operacionDivision = false;
    }

    public String getDigito() {
        return digito;
    }

    public void setDigito(String digito) {
        this.digito = digito;
    }
    
    public void concatenarNumero(Integer numero){
        setDigito(digito + numero);
    }
    
    public void suma(String numero){
        this.resultado = Double.parseDouble(numero);
        this.operacionSuma = true;
        setDigito("");
    }
    
    public void resta(String numero){
        this.resultado = Double.parseDouble(numero);
        this.operacionResta = true;
        setDigito("");
    }
    
    public void multiplicacion(String numero){
        this.resultado = Double.parseDouble(numero);
        this.operacionMultiplicacion = true;
        setDigito("");
    }
    
    public void division(String numero){
        this.resultado = Double.parseDouble(numero);
        this.operacionDivision = true;
        setDigito("");
    }
    
    public void borrar(){
        if(this.digito.length() > 0)
            this.digito = this.digito.substring(0, this.digito.length()-1);
    }
    
    public double resultadoOperacion(String numero){
        this.digito = "";
        if(this.operacionSuma){
            this.operacionSuma = false;
            return (this.resultado + Double.parseDouble(numero));
        }
        if(this.operacionResta){
            this.operacionResta = false;
            return (this.resultado - Double.parseDouble(numero));   
        }
        if(this.operacionMultiplicacion){
            this.operacionMultiplicacion = false;
            return (this.resultado * Double.parseDouble(numero));   
        }
        if(this.operacionDivision){
            this.operacionDivision = false;
            return (this.resultado / Double.parseDouble(numero));   
        }
        return 0;
    }
    
}
